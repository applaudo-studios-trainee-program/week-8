import { dataFetcher } from 'services/client';
import { StoriesResponse, Stories, Story } from 'interfaces/stories';
import { StoriesFilters } from 'reducers/storiesReducer';

type GetStoriesProps = { offset: number; filter?: Required<StoriesFilters> };

export const getStories = async ({ offset, filter }: GetStoriesProps) => {
  const urlParams = filter
    ? `offset=${offset}&${filter.filterName}=${encodeURI(filter.filterValue)}`
    : `offset=${offset}`;

  const response = await dataFetcher({ endpoint: 'stories', urlParams });

  const { data }: StoriesResponse = await response.json();

  const results: Stories[] = data.results.map(({ id, title, description }) => ({
    id,
    title,
    description
  }));

  return {
    data: results,
    limit: data.limit,
    offset: data.offset,
    total: data.total
  };
};

export const getStoriesIdAndName = async () => {
  const urlParams = 'limit=100';

  const response = await dataFetcher({ endpoint: 'comics', urlParams });

  const { data }: StoriesResponse = await response.json();

  return data.results.map(({ id, title }) => ({
    value: id.toString(),
    label: title
  }));
};

type GetStoryByIdProps = { id: string };

export const getStoryById = async ({ id }: GetStoryByIdProps) => {
  const response = await dataFetcher({ endpoint: `comics/${id}` });

  const { data }: StoriesResponse = await response.json();

  const results: Story = data.results.map(
    ({ id, title, description, comics }) => ({
      comicItems: comics?.items,
      description,
      id,
      title
    })
  )[0];

  return results;
};
