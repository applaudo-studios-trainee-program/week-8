import { dataFetcher } from 'services/client';

import {
  Character,
  Characters,
  CharactersResponse
} from 'interfaces/characters';

import { CharactersFilters } from 'reducers/charactersReducer';

type GetCharactersProps = {
  offset: number;
  filter?: Required<CharactersFilters>;
};

export const getCharacters = async ({ offset, filter }: GetCharactersProps) => {
  const urlParams = filter
    ? `offset=${offset}&${filter.filterName}=${encodeURI(filter.filterValue)}`
    : `offset=${offset}`;

  const response = await dataFetcher({ endpoint: 'characters', urlParams });

  const { data }: CharactersResponse = await response.json();

  const results: Characters[] = data.results.map(({ id, name, thumbnail }) => {
    const { extension, path } = thumbnail;

    return {
      id,
      name,

      thumbnail: `${path}/landscape_incredible.${extension}`
    };
  });

  return {
    data: results,
    limit: data.limit,
    offset: data.offset,
    total: data.total
  };
};

type GetCharactersByNameProps = { characterName: string };

export const getCharactersByName = async ({
  characterName
}: GetCharactersByNameProps) => {
  const urlParams = `nameStartsWith=${characterName}&orderBy=name`;

  const response = await dataFetcher({ endpoint: 'characters', urlParams });

  const { data }: CharactersResponse = await response.json();

  const results: Characters[] = data.results.map(({ id, name, thumbnail }) => {
    const { extension, path } = thumbnail;

    return {
      id,
      name,
      thumbnail: `${path}/landscape_incredible.${extension}`
    };
  });

  return {
    data: results,
    limit: data.limit,
    offset: data.offset,
    total: data.total
  };
};

type GetCharacterByIdProps = { id: string };

export const getCharacterById = async ({ id }: GetCharacterByIdProps) => {
  const response = await dataFetcher({ endpoint: `characters/${id}` });

  const { data }: CharactersResponse = await response.json();

  const results: Character = data.results.map(
    ({ id, name, description, thumbnail, stories, comics }) => {
      const { extension, path } = thumbnail;
      const { items: comicsItems } = comics;
      const { items: storiesItems } = stories;

      return {
        id,
        name,
        description,
        comicsItems,
        storiesItems,
        thumbnail: `${path}/standard_fantastic.${extension}`
      };
    }
  )[0];

  return results;
};
