const BASE_URL = process.env.REACT_APP_BASE_URL;
const API_CONFIG = process.env.REACT_APP_API_CONFIG;

type Props = {
  endpoint: string;
  data?: { [key: string]: string };
  method?: 'GET' | 'POST' | 'PUT' | 'DELETE';
  urlParams?: string;
};

export const dataFetcher = ({
  endpoint,
  urlParams,
  data = {},
  method = 'GET'
}: Props) => {
  const URL = urlParams
    ? `${BASE_URL}${endpoint}?${API_CONFIG}&${urlParams}`
    : `${BASE_URL}${endpoint}?${API_CONFIG}`;

  if (method === 'GET') return fetch(URL);

  return fetch(URL, {
    method,
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data)
  });
};
