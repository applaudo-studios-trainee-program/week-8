export interface PaginationData<T> {
  count: number;
  limit: number;
  data: T[];
}

export interface Filters<T> {
  filterName?: T;
  filterValue?: string;
}
