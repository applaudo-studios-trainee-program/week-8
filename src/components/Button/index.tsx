import { ReactNode, memo } from 'react';
import styled from '@emotion/styled';

type LayoutProps = { shouldRemoveBackground?: boolean };

type Props = {
  type: 'button' | 'submit';
  children: ReactNode;

  disabled?: boolean;

  onClick?: () => void;
} & LayoutProps;

const StyledButton = styled.button<LayoutProps>`
  ${({ shouldRemoveBackground }) =>
    shouldRemoveBackground
      ? 'background-color: transparent;'
      : 'background-color: var(--primary-white-color);'}

  border-radius: var(--border-radius);

  display: flex;

  align-items: center;
  justify-content: center;

  gap: 1rem;

  width: max-content;
`;

const Button = ({
  type,
  children,
  shouldRemoveBackground,
  disabled,
  onClick
}: Props) => (
  <StyledButton
    shouldRemoveBackground={shouldRemoveBackground}
    type={type === 'button' ? 'button' : 'submit'}
    disabled={disabled}
    onClick={onClick}
  >
    {children}
  </StyledButton>
);

export default memo(Button);
