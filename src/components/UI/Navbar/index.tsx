import { useState, memo } from 'react';
import { Link, NavLink } from 'react-router-dom';

import { Paths } from 'components/App';

import BurgerMenu from 'assets/icons/BurgerMenu';

import Styles from './Navbar.module.css';

const Navbar = () => {
  const [showMenu, setShowMenu] = useState(false);

  const handleIconClick = () => setShowMenu(showMenu => !showMenu);

  const handleAnchorClick = () => {
    setShowMenu(showMenu => showMenu && !showMenu);
  };

  return (
    <header className={Styles.container}>
      <nav className={Styles.menu}>
        <Link
          to={Paths.HomePath}
          style={{ height: 'min-content' }}
          onClick={handleAnchorClick}
        >
          <h2>Marvel DOCS</h2>
        </Link>

        <ul
          className={
            showMenu
              ? `${Styles['menu-list']} ${Styles.show}`
              : Styles['menu-list']
          }
        >
          <li className={Styles['menu-item']}>
            <NavLink
              className={Styles['menu-anchor']}
              to={Paths.HomePath}
              onClick={handleAnchorClick}
            >
              Home
            </NavLink>
          </li>
          <li className={Styles['menu-item']}>
            <NavLink
              className={Styles['menu-anchor']}
              to={Paths.CharacterListPath}
              onClick={handleAnchorClick}
            >
              Characters
            </NavLink>
          </li>
          <li className={Styles['menu-item']}>
            <NavLink
              className={Styles['menu-anchor']}
              to={Paths.ComicListPath}
              onClick={handleAnchorClick}
            >
              Comics
            </NavLink>
          </li>
          <li className={Styles['menu-item']}>
            <NavLink
              className={Styles['menu-anchor']}
              to={Paths.StoryListPath}
              onClick={handleAnchorClick}
            >
              Stories
            </NavLink>
          </li>
          <li className={Styles['menu-item']}>
            <NavLink
              className={Styles['menu-anchor']}
              to={Paths.BookmarksPath}
              onClick={handleAnchorClick}
            >
              Bookmarks
            </NavLink>
          </li>
        </ul>

        <BurgerMenu className={Styles['menu-icon']} onClick={handleIconClick} />
      </nav>
    </header>
  );
};

export default memo(Navbar);
