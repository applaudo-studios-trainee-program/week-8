import { lazy, memo } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import { Paths } from 'components/App';

import Container from 'components/Container';

const CharacterDetails = lazy(
  () => import(/* webpackPrefetch: true */ 'views/Details/CharacterDetails')
);

const ComicDetails = lazy(
  () => import(/* webpackPrefetch: true */ 'views/Details/ComicDetails')
);

const StoryDetails = lazy(
  () => import(/* webpackPrefetch: true */ 'views/Details/StoryDetails')
);

const DetailsLayout = () => (
  <Container maxWidthInREM={60}>
    <Switch>
      <Route
        exact
        path={Paths.CharacterDetailsPath}
        component={CharacterDetails}
      />

      <Route exact path={Paths.ComicDetailsPath} component={ComicDetails} />
      <Route exact path={Paths.StoryDetailsPath} component={StoryDetails} />

      <Redirect to="/" />
    </Switch>
  </Container>
);

export default memo(DetailsLayout);
