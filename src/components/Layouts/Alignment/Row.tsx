import { memo } from 'react';
import styled from '@emotion/styled';

type LayoutProps = { gap?: number; flex?: boolean };

const Row = styled.div<LayoutProps>`
  display: flex;

  ${({ flex }) => (flex ? 'flex: 1;' : null)}
  flex-direction: row;
  flex-wrap: wrap;

  ${({ gap }) => (gap ? `gap: ${gap}rem;` : null)}
`;

export default memo(Row);
