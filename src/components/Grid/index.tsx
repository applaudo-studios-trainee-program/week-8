import { ReactNode, memo } from 'react';
import styled from '@emotion/styled';

type LayoutProps = { gap: number };

type Props = { children: ReactNode } & LayoutProps;

const StyledGrid = styled.div<LayoutProps>`
  display: grid;

  gap: ${({ gap }) => gap}rem;

  grid-auto-rows: min-content;
  grid-template-columns: 1fr;

  @media (min-width: 576px) {
    grid-template-columns: repeat(2, 1fr);
  }

  @media (min-width: 992px) {
    grid-template-columns: repeat(4, 1fr);
  }
`;

const Grid = ({ gap, children }: Props) => (
  <StyledGrid gap={gap}>{children}</StyledGrid>
);

export default memo(Grid);
