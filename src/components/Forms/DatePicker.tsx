import { ChangeEvent } from 'react';
import styled from '@emotion/styled';

type Props = {
  value?: string;
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
};

const StyledDatePicker = styled.input`
  border: none;
  border-radius: var(--border-radius);

  height: 3.8rem;

  outline: none;

  padding: 0.5rem 2rem;

  &:focus {
    box-shadow: 0 0 0 0.2rem #2684ff;
  }
`;

const DatePicker = ({ value, onChange }: Props) => (
  <StyledDatePicker type="date" value={value} onChange={onChange} />
);

export default DatePicker;
