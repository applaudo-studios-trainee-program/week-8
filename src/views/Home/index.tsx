import Column from 'components/Layouts/Alignment/Column';
import Row from 'components/Layouts/Alignment/Row';
import Container from 'components/Container';
import Hero from 'components/UI/Hero';

const Home = () => (
  <Column gap={2}>
    <Hero />

    <Container>
      <Column gap={2}>
        <Row flex gap={2}>
          <Column flex gap={2} style={{ padding: '3rem' }}>
            <h2>What is Marvel?</h2>

            <span>
              Marvel Comics is an American media and entertainment company
              regarded as one of the “big two” publishers in the comics
              industry. Its parent company, Marvel Entertainment, is a wholly
              owned subsidiary of the Disney Company. Its headquarters are in
              New York City.
            </span>
          </Column>

          <Column flex gap={2} style={{ padding: '3rem' }}>
            <h2>Who was Marvel Comics’ first original character?</h2>

            <span>
              The first comic book issued by Marvel’s precursor Timely Comics,
              in October 1939, featured the debut of the characters Human Torch,
              Sub-Mariner, the Angel, Ka-Zar, and the Masked Raider.
            </span>
          </Column>
        </Row>

        <Row flex>
          <Column flex gap={2} style={{ padding: '3rem' }}>
            <h2>Where did Marvel Comics get its name?</h2>

            <span>
              The title of Timely Comics’ first comic book, in 1939, was Marvel
              Comics no. 1. The company continued to release comics under the
              Marvel name through the 1940s and ’50s. Timely changed its name to
              Atlas Comics in 1951 and became Marvel Comics in the early 1960s,
              recalling the title it had often used.
            </span>
          </Column>
        </Row>
      </Column>
    </Container>
  </Column>
);

export default Home;
