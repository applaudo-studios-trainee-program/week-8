import {
  ChangeEvent,
  Dispatch,
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState
} from 'react';

import { debounce } from 'lodash';

import Select, { OptionTypeBase, ValueType } from 'react-select';

import { getStories } from 'services/entities/stories';

import { removeFilter, setData, setFilterData } from 'actions/stories';

import {
  StoriesActions,
  StoriesFilters,
  StoriesState
} from 'reducers/storiesReducer';

import { filterStoriesOptions } from 'data';

import { StoriesContext } from 'contexts/StoriesContext';

import Column from 'components/Layouts/Alignment/Column';
import Row from 'components/Layouts/Alignment/Row';
import DatePicker from 'components/Forms/DatePicker';
import GenericPagination from 'components/GenericPagination';

import 'rc-pagination/assets/index.css';

const StoryList = () => {
  const isMounted = useRef(false);

  const [inputValue, setInputValue] = useState('');

  const { storiesState, dispatchToStories } = useContext(StoriesContext) as {
    storiesState: StoriesState;
    dispatchToStories: Dispatch<StoriesActions>;
  };

  const { data, filterData, offset } = storiesState;

  useEffect(() => {
    getStories({ offset: 0 }).then(
      response =>
        isMounted.current &&
        dispatchToStories(setData({ ...response, offset: 1 }))
    );

    return () => {
      dispatchToStories(removeFilter());
    };
  }, []);

  useEffect(() => {
    isMounted.current = true;

    return () => {
      isMounted.current = false;
    };
  }, []);

  const handleDebouncedSearch = useCallback(
    debounce((filter: Required<StoriesFilters>) => {
      getStories({ offset: 0, filter }).then(
        response =>
          isMounted.current &&
          dispatchToStories(setData({ ...response, offset: 1 }))
      );
    }, 1000),
    []
  );

  const handlePageChange = (page: number) => {
    if (filterData) {
      getStories({
        offset: (page - 1) * 20,
        filter: filterData as Required<StoriesFilters>
      }).then(
        response =>
          isMounted.current &&
          dispatchToStories(setData({ ...response, offset: page }))
      );
    } else {
      getStories({ offset: (page - 1) * 20 }).then(
        response =>
          isMounted.current &&
          dispatchToStories(setData({ ...response, offset: page }))
      );
    }
  };

  const handleDateChange = ({ target }: ChangeEvent<HTMLInputElement>) => {
    const input = target as HTMLInputElement;
    const inputValue = input.value;

    setInputValue(inputValue);

    dispatchToStories(
      setFilterData({ filterData: { filterValue: inputValue } })
    );

    const filterInfo: StoriesFilters = {
      ...filterData,
      filterValue: inputValue
    };

    handleDebouncedSearch(filterInfo as Required<StoriesFilters>);
  };

  const handleFilterOption = (
    selectedValue: ValueType<OptionTypeBase, boolean>
  ) => {
    if (selectedValue) {
      const { value } = selectedValue as {
        value: 'modifiedSince';
      };

      dispatchToStories(setFilterData({ filterData: { filterName: value } }));
    } else {
      dispatchToStories(removeFilter());

      getStories({ offset: 0 }).then(
        response =>
          isMounted.current &&
          dispatchToStories(setData({ ...response, offset: 1 }))
      );
    }
  };

  if (!isMounted.current) {
    return (
      <>
        {!data.length ? (
          <h1
            style={{
              color: 'var(--secondary-white-color)',
              letterSpacing: '0.02em'
            }}
          >
            Loading Data...
          </h1>
        ) : null}
      </>
    );
  }

  return (
    <Column gap={2}>
      <h1>Stories List</h1>

      <Row gap={3}>
        <Column flex gap={1}>
          <span>Filter Options</span>
          <Select
            defaultValue={filterStoriesOptions[0]}
            options={filterStoriesOptions}
            isClearable
            onChange={handleFilterOption}
          />
        </Column>

        {filterData ? (
          <Column flex gap={1}>
            {filterData.filterName === 'modifiedSince' ? (
              <>
                <span>By Date</span>
                <DatePicker value={inputValue} onChange={handleDateChange} />
              </>
            ) : null}
          </Column>
        ) : null}
      </Row>

      <GenericPagination
        {...storiesState}
        type="stories"
        limit={20}
        onPageChange={handlePageChange}
      />
    </Column>
  );
};

export default StoryList;
