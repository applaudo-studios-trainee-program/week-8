import { Dispatch, useContext, useEffect, useState } from 'react';
import { RouteComponentProps, useParams } from 'react-router-dom';
import styled from '@emotion/styled';

import { addBookmark, removeBookmark } from 'actions/bookmarks';
import { addEntryToHide } from 'actions/comics';

import { BookmarksActions, BookmarksState } from 'reducers/bookmarksReducer';
import { ComicsActions } from 'reducers/comicsReducer';

import { BookmarksContext } from 'contexts/BookmarksContext';
import { ComicsContext } from 'contexts/ComicsContext';

import { Comic } from 'interfaces/comics';

import { getComicById } from 'services/entities/comics';

import Button from 'components/Button';
import Column from 'components/Layouts/Alignment/Column';
import Row from 'components/Layouts/Alignment/Row';
import Heart from 'assets/icons/Heart';
import Remove from 'assets/icons/Remove';
import ItemList from 'components/ItemList';

const StyledImageCard = styled.img`
  border-radius: var(--border-radius);
`;

const ComicDetails = ({ history }: RouteComponentProps) => {
  const { id } = useParams<{ id: string }>();

  const [comicData, setComicData] = useState<Comic | {}>({});

  const { bookmarksState, dispatchToBookmarks } = useContext(
    BookmarksContext
  ) as {
    bookmarksState: BookmarksState;
    dispatchToBookmarks: Dispatch<BookmarksActions>;
  };

  const { comicBookmarks } = bookmarksState;

  const { dispatchToComics } = useContext(ComicsContext) as {
    dispatchToComics: Dispatch<ComicsActions>;
  };

  useEffect(() => {
    getComicById({ id }).then(setComicData);
  }, []);

  const handleBookmark = () => {
    const isBookmarkAlreadyRegistered = !!comicBookmarks.find(
      comicId => comicId === +id
    );

    if (!isBookmarkAlreadyRegistered) {
      dispatchToBookmarks(
        addBookmark({ bookmarkListName: 'comicBookmarks', bookmarkId: +id })
      );
    } else dispatchToBookmarks(removeBookmark({ bookMarkId: +id }));
  };

  const handleHide = () => {
    dispatchToComics(addEntryToHide({ comicId: +id }));

    dispatchToBookmarks(removeBookmark({ bookMarkId: +id }));

    history.push('/list/comics');
  };

  if (!Object.keys(comicData).length) {
    return (
      <h1
        style={{
          color: 'var(--secondary-white-color)',
          letterSpacing: '0.02em'
        }}
      >
        Loading Data...
      </h1>
    );
  }

  const {
    title,
    thumbnail,
    description,
    creatorItems,
    characterItems
  } = comicData as Comic;

  return (
    <Column gap={2}>
      <h1>{title}</h1>

      <StyledImageCard
        alt={`${title} Thumbnail`}
        loading="lazy"
        src={thumbnail}
        title={`${title} Thumbnail`}
      />

      {description ? (
        <Column gap={1}>
          <h3>Description</h3>

          <span>{description}</span>
        </Column>
      ) : null}

      {creatorItems.length ? (
        <Column gap={1}>
          <h3>Creator(s)</h3>

          <ItemList dataToList={creatorItems} />
        </Column>
      ) : null}

      {characterItems.length ? (
        <Column gap={1}>
          <h3>Characters(s)</h3>

          <ItemList dataToList={characterItems} />
        </Column>
      ) : null}

      <Row gap={2}>
        <Button type="button" onClick={handleBookmark} shouldRemoveBackground>
          <Heart
            shouldStaySameColor={
              !!comicBookmarks.find(charaterId => charaterId === +id)
            }
          />{' '}
          <span style={{ color: 'var(--primary-white-color)' }}>Save</span>
        </Button>

        <Button type="button" onClick={handleHide} shouldRemoveBackground>
          <Remove />{' '}
          <span style={{ color: 'var(--primary-white-color)' }}>Hide</span>
        </Button>
      </Row>
    </Column>
  );
};

export default ComicDetails;
