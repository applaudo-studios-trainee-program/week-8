import { Dispatch, useContext, useEffect, useState } from 'react';
import { RouteComponentProps, useParams, withRouter } from 'react-router-dom';
import styled from '@emotion/styled';

import { addBookmark, removeBookmark } from 'actions/bookmarks';
import { addEntryToHide } from 'actions/characters';

import { BookmarksActions, BookmarksState } from 'reducers/bookmarksReducer';

import { BookmarksContext } from 'contexts/BookmarksContext';
import { CharactersContext } from 'contexts/CharactersContext';

import { Character } from 'interfaces/characters';

import { CharactersActions } from 'reducers/charactersReducer';

import { getCharacterById } from 'services/entities/characters';

import Button from 'components/Button';
import Column from 'components/Layouts/Alignment/Column';
import Row from 'components/Layouts/Alignment/Row';
import Heart from 'assets/icons/Heart';
import Remove from 'assets/icons/Remove';
import ItemList from 'components/ItemList';

const StyledImageCard = styled.img`
  border-radius: var(--border-radius);
`;

const CharacterDetails = ({ history }: RouteComponentProps) => {
  const { id } = useParams<{ id: string }>();

  const [characterData, setCharacterData] = useState<Character | {}>({});

  const { bookmarksState, dispatchToBookmarks } = useContext(
    BookmarksContext
  ) as {
    bookmarksState: BookmarksState;
    dispatchToBookmarks: Dispatch<BookmarksActions>;
  };

  const { dispatchToCharacters } = useContext(CharactersContext) as {
    dispatchToCharacters: Dispatch<CharactersActions>;
  };

  const { characterBookmarks } = bookmarksState;

  useEffect(() => {
    getCharacterById({ id }).then(setCharacterData);
  }, []);

  const handleBookmark = () => {
    const isBookmarkAlreadyRegistered = !!characterBookmarks.find(
      charaterId => charaterId === +id
    );

    if (!isBookmarkAlreadyRegistered) {
      dispatchToBookmarks(
        addBookmark({ bookmarkListName: 'characterBookmarks', bookmarkId: +id })
      );
    } else dispatchToBookmarks(removeBookmark({ bookMarkId: +id }));
  };

  const handleHide = () => {
    dispatchToCharacters(addEntryToHide({ characterId: +id }));

    dispatchToBookmarks(removeBookmark({ bookMarkId: +id }));

    history.push('/list/characters');
  };

  if (!Object.keys(characterData).length) {
    return (
      <h1
        style={{
          color: 'var(--secondary-white-color)',
          letterSpacing: '0.02em'
        }}
      >
        Loading Data...
      </h1>
    );
  }

  const {
    name,
    thumbnail,
    description,
    comicsItems,
    storiesItems
  } = characterData as Character;

  return (
    <Column gap={2}>
      <h1>{name}</h1>

      <StyledImageCard
        alt={`${name} Thumbnail`}
        loading="lazy"
        src={thumbnail}
        title={`${name} Thumbnail`}
      />

      {description ? (
        <Column gap={1}>
          <h3>Description</h3>

          <span>{description}</span>
        </Column>
      ) : null}

      {comicsItems.length ? (
        <Column gap={1}>
          <h3>Comics</h3>

          <ItemList dataToList={comicsItems} />
        </Column>
      ) : null}

      {storiesItems.length ? (
        <Column gap={1}>
          <h3>Stories</h3>

          <ItemList dataToList={storiesItems} />
        </Column>
      ) : null}

      <Row gap={2}>
        <Button type="button" onClick={handleBookmark} shouldRemoveBackground>
          <Heart
            shouldStaySameColor={
              !!characterBookmarks.find(charaterId => charaterId === +id)
            }
          />{' '}
          <span style={{ color: 'var(--primary-white-color)' }}>Save</span>
        </Button>

        <Button type="button" onClick={handleHide} shouldRemoveBackground>
          <Remove />{' '}
          <span style={{ color: 'var(--primary-white-color)' }}>Hide</span>
        </Button>
      </Row>
    </Column>
  );
};

export default withRouter(CharacterDetails);
