import { Dispatch, useContext, useEffect, useState } from 'react';
import { RouteComponentProps, useParams } from 'react-router-dom';

import { addBookmark, removeBookmark } from 'actions/bookmarks';
import { addEntryToHide } from 'actions/stories';

import { BookmarksActions, BookmarksState } from 'reducers/bookmarksReducer';
import { StoriesActions } from 'reducers/storiesReducer';

import { BookmarksContext } from 'contexts/BookmarksContext';
import { StoriesContext } from 'contexts/StoriesContext';

import { getStoryById } from 'services/entities/stories';

import { Story } from 'interfaces/stories';

import Button from 'components/Button';
import Column from 'components/Layouts/Alignment/Column';
import Row from 'components/Layouts/Alignment/Row';
import Heart from 'assets/icons/Heart';
import Remove from 'assets/icons/Remove';
import ItemList from 'components/ItemList';

const StoryDetails = ({ history }: RouteComponentProps) => {
  const { id } = useParams<{ id: string }>();

  const [storyData, setStoryData] = useState<Story | {}>({});

  const { bookmarksState, dispatchToBookmarks } = useContext(
    BookmarksContext
  ) as {
    bookmarksState: BookmarksState;
    dispatchToBookmarks: Dispatch<BookmarksActions>;
  };

  const { storiesBookmarks } = bookmarksState;

  const { dispatchToStories } = useContext(StoriesContext) as {
    dispatchToStories: Dispatch<StoriesActions>;
  };

  useEffect(() => {
    getStoryById({ id }).then(setStoryData);
  }, []);

  const handleBookmark = () => {
    const isBookmarkAlreadyRegistered = !!storiesBookmarks.find(
      storiesId => storiesId === +id
    );

    if (!isBookmarkAlreadyRegistered) {
      dispatchToBookmarks(
        addBookmark({ bookmarkListName: 'storiesBookmarks', bookmarkId: +id })
      );
    } else dispatchToBookmarks(removeBookmark({ bookMarkId: +id }));
  };

  const handleHide = () => {
    dispatchToStories(addEntryToHide({ storyId: +id }));

    dispatchToBookmarks(removeBookmark({ bookMarkId: +id }));

    history.push('/list/stories');
  };

  if (!Object.keys(storyData).length) {
    return (
      <h1
        style={{
          color: 'var(--secondary-white-color)',
          letterSpacing: '0.02em'
        }}
      >
        Loading Data...
      </h1>
    );
  }

  const { title, description, comicItems } = storyData as Story;

  return (
    <Column gap={2}>
      <h1>{title}</h1>

      {description ? (
        <Column gap={1}>
          <h3>Description</h3>

          <span>{description}</span>
        </Column>
      ) : null}

      {comicItems?.length ? (
        <Column gap={1}>
          <h3>Comics</h3>

          <ItemList dataToList={comicItems} />
        </Column>
      ) : null}

      <Row gap={2}>
        <Button type="button" onClick={handleBookmark} shouldRemoveBackground>
          <Heart
            shouldStaySameColor={
              !!storiesBookmarks.find(charaterId => charaterId === +id)
            }
          />{' '}
          <span style={{ color: 'var(--primary-white-color)' }}>Save</span>
        </Button>

        <Button type="button" onClick={handleHide} shouldRemoveBackground>
          <Remove />{' '}
          <span style={{ color: 'var(--primary-white-color)' }}>Hide</span>
        </Button>
      </Row>
    </Column>
  );
};

export default StoryDetails;
