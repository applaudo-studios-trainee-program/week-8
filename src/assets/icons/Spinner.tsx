import styled from '@emotion/styled';

const StyledSpinnerContainer = styled.div`
  display: flex;

  align-items: center;
  justify-content: center;

  height: 100%;
  width: 100%;

  position: fixed;
`;

const StyledSpinner = styled.div`
  animation: spin 1s ease infinite;

  border: 0.5rem solid rgba(0, 0, 0, 0.1);
  border-left-color: #09f;
  border-radius: 50%;

  height: 4em;
  width: 4em;

  @keyframes spin {
    0% {
      transform: rotate(0deg);
    }

    100% {
      transform: rotate(360deg);
    }
  }
`;

const Spinner = () => (
  <StyledSpinnerContainer>
    <StyledSpinner />
  </StyledSpinnerContainer>
);

export default Spinner;
