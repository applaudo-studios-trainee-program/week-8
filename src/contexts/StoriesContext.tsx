import { ReactNode, Dispatch, useReducer, createContext } from 'react';

import {
  StoriesActions,
  storiesReducer,
  StoriesState
} from 'reducers/storiesReducer';

type Props = { children: ReactNode };

const initialState: StoriesState = {
  data: [],
  offset: 0,
  total: 0,
  filterData: null,
  removedElements: []
};

export const StoriesContext = createContext<{
  storiesState: StoriesState;
  dispatchToStories: Dispatch<StoriesActions>;
} | null>(null);

export const StoriesContextProvider = ({ children }: Props) => {
  const [storiesState, dispatchToStories] = useReducer(
    storiesReducer,
    initialState
  );

  return (
    <StoriesContext.Provider value={{ storiesState, dispatchToStories }}>
      {children}
    </StoriesContext.Provider>
  );
};
