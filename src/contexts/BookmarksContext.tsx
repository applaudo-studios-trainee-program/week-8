import { ReactNode, createContext, Dispatch } from 'react';

import { BookmarksState, BookmarksActions } from 'reducers/bookmarksReducer';

type Props = { children: ReactNode; initFn?: () => void };

export const BookmarksContext = createContext<{
  bookmarksState: BookmarksState;
  dispatchToBookmarks: Dispatch<BookmarksActions>;
} | null>(null);
