import { initialBookmarksState } from 'components/App';

import {
  BookmarksActions,
  BookmarksTypes,
  BookmarksState
} from 'reducers/bookmarksReducer';

type AddBookmarkProps = {
  bookmarkId: number;
  bookmarkListName: BookmarksTypes;
};

export const addBookmark = ({
  bookmarkId,
  bookmarkListName
}: AddBookmarkProps): BookmarksActions => {
  const bookmarks: BookmarksState = JSON.parse(
    localStorage.getItem('bookmarks') || 'null'
  );

  bookmarks[bookmarkListName].push(bookmarkId);

  localStorage.setItem('bookmarks', JSON.stringify(bookmarks));

  return {
    type: 'ADD_BOOKMARK',
    payload: { bookmarkId, bookmarkListName }
  };
};

type RemoveBookmarkProps = { bookMarkId: number };

export const removeBookmark = ({
  bookMarkId
}: RemoveBookmarkProps): BookmarksActions => {
  const bookmarks: BookmarksState = JSON.parse(
    localStorage.getItem('bookmarks') || 'null'
  );

  const newBookmarks: BookmarksState = {
    characterBookmarks: bookmarks.characterBookmarks.filter(
      characterId => characterId !== bookMarkId
    ),

    comicBookmarks: bookmarks.comicBookmarks.filter(
      comicId => comicId !== bookMarkId
    ),

    storiesBookmarks: bookmarks.storiesBookmarks.filter(
      storyId => storyId !== bookMarkId
    )
  };

  localStorage.setItem('bookmarks', JSON.stringify(newBookmarks));

  return {
    type: 'REMOVE_BOOKMARK',
    payload: { bookMarkId }
  };
};

export const removeAllBookmarks = (): BookmarksActions => {
  localStorage.setItem('bookmarks', JSON.stringify(initialBookmarksState));

  return {
    type: 'REMOVE_ALL_BOOKMARKS'
  };
};
