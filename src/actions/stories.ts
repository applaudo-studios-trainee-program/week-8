import { StoriesState, StoriesActions } from 'reducers/storiesReducer';

export const setData = ({
  data,
  offset,
  total
}: Pick<StoriesState, 'data' | 'offset' | 'total'>): StoriesActions => ({
  type: 'SET_DATA',
  payload: { data, offset, total }
});

export const setFilterData = ({
  filterData
}: Pick<StoriesState, 'filterData'>): StoriesActions => ({
  type: 'SET_FILTER_DATA',
  payload: { filterData }
});

export const removeFilter = (): StoriesActions => ({ type: 'REMOVE_FILTER' });

type AddEntryToHideProps = { storyId: number };

export const addEntryToHide = ({
  storyId
}: AddEntryToHideProps): StoriesActions => ({
  type: 'ADD_ENTRY_TO_HIDE',
  payload: { storyId }
});
